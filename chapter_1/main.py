from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def index():
    return {"message": "Hello"}


# if __name__ == "__main__":
#     uvicorn.run("main:app", port=5000, log_level="info")
