from fastapi import FastAPI
from datamodels import Productbm as Product
from datamodels import User, Employee, Company

app = FastAPI()

productlist = []


# @app.post("/product")
# async def addnew(
#     request: Request,
#     prodId: int = Body(),
#     prodName: str = Body(),
#     price: float = Body(),
#     stock: int = Body(),
# ):
#     product = {
#         "Product ID": prodId,
#         "product name": prodName,
#         "Price": price,
#         "Stock": stock,
#     }
#     return product
@app.post("/product")
async def addnew(product: Product):
    dct = product.model_dump()
    price = dct["price"]
    if price > 5000:
        dct["price"] = price + price * 0.1
        product.price = dct["price"]
    productlist.append(product)
    return productlist


@app.post("/student")
async def addnew(user: User):
    return user


@app.get("/get_prod")
async def get_prod():
    return productlist


@app.post("/employee")
async def addnewemp(emp: Employee):
    return emp


@app.post("/company")
async def add_company(comp: Company):
    return comp
