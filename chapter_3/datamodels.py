from dataclasses import dataclass
from pydantic import BaseModel, SecretStr, Json, HttpUrl, validator
from typing import Dict, List
from sqlalchemy import Column, Integer, Float, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class ProductORM(Base):
    __tablename__ = "products"
    prodId = Column(Integer, primary_key=True, nullable=False)
    prodName = Column(String(63), unique=True)
    price = Column(Float)
    stock = Column(Integer)


@dataclass
class Productdc:
    prodId: int
    prodName: str
    price: float
    stock: int


class Productbm(BaseModel):
    prodId: int
    prodName: str
    price: float
    stock: int

    class Config:
        from_attributes = True
        json_schema_extra = {
            "example": {"prodId": 1, "prodName": "Fan", "price": 2000, "stock": 50}
        }


class User(BaseModel):
    UserID: int
    name: str
    subjects: Dict[str, int]


class Company(BaseModel):
    CompanyID: int
    users: List[User]


class Employee(BaseModel):
    ID: str
    password: SecretStr
    salary: float
    details: Json
    VKProfile: HttpUrl

    @validator("ID")
    def alphanum(cls, x: str):
        if x.isalnum() is False:
            raise ValueError("Нужно намерик быть")


# prod_alchemy = ProductORM(prodId=1, prodName="fan", price=2000, stock=50)
# product = Productbm.model_validate(prod_alchemy)
# print(product)
