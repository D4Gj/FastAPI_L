# Обучение FastAPI

`Прочитано 241/321`

## Часть 1 

### Изучил, узнал принцип работы, дополнил знания, повторил:

* ASGI
* Uvicorn
* REST
* HTTP
* FastAPI Dependencies(Starlette routing, Pydantic, OpenAPI)
* Базовый запуск FastAPI и uvicorn

## Часть 2 

### Изучил, узнал принцип работы, дополнил знания, повторил:

* Swagger
* Redoc
* JSON Schema
* Path parameters
* Type Hinting and Parsing for FastAPI
* Query parameters
* Optional parameters
* Validation (Numeric, RegEx, Length)
* Metadata

## Часть 3

### Изучил, узнал принцип работы, дополнил знания, повторил:

* Pydantic
* datamodels
* POST, GET with data datamodels

## Часть 4

### Изучил, узнал принцип работы, дополнил знания, повторил:

* Templates
* Static
* Scripts


## Часть 5

### Изучил, узнал принцип работы, дополнил знания, повторил:

* Cookies
* Form
* HTTP Status Code
* HTMLResponse
* JSONResponse
* StreamingResponse
* RedirectResponse
* SqlAlchemy
* APIRouter
* CORS
* Мидлвари

## Часть 6

### Изучил, узнал принцип работы, дополнил знания, повторил:

* Websockets
* Security things
* Hashing
* Types of documentation
* Alchemy relationships
* JWT