## FastAPI Best Practices
Как написать код, чтобы потом на него можно было смотреть с кайфом.

### Contents TODO
1. [Project Structure. Consistent & predictable.](https://github.com/zhanymkanov/fastapi-best-practices#1-project-structure-consistent--predictable)
2. [Excessively use Pydantic for data validation.](https://github.com/zhanymkanov/fastapi-best-practices#2-excessively-use-pydantic-for-data-validation)
3. [Use dependencies for data validation vs DB.](https://github.com/zhanymkanov/fastapi-best-practices#3-use-dependencies-for-data-validation-vs-db)
4. [Chain dependencies.](https://github.com/zhanymkanov/fastapi-best-practices#4-chain-dependencies)
5. [Decouple & Reuse dependencies. Dependency calls are cached.](https://github.com/zhanymkanov/fastapi-best-practices#5-decouple--reuse-dependencies-dependency-calls-are-cached)
6. [Follow the REST.](https://github.com/zhanymkanov/fastapi-best-practices#6-follow-the-rest)
7. [Don't make your routes async, if you have only blocking I/O operations.](https://github.com/zhanymkanov/fastapi-best-practices#7-dont-make-your-routes-async-if-you-have-only-blocking-io-operations)
8. [Custom base model from day 0.](https://github.com/zhanymkanov/fastapi-best-practices#8-custom-base-model-from-day-0)
9. [Docs.](https://github.com/zhanymkanov/fastapi-best-practices#9-docs)
10. [Use Pydantic's BaseSettings for configs.](https://github.com/zhanymkanov/fastapi-best-practices#10-use-pydantics-basesettings-for-configs)
11. [SQLAlchemy: Set DB keys naming convention.](https://github.com/zhanymkanov/fastapi-best-practices#11-sqlalchemy-set-db-keys-naming-convention)
12. [Migrations. Alembic.](https://github.com/zhanymkanov/fastapi-best-practices#12-migrations-alembic)
13. [Set DB naming convention.](https://github.com/zhanymkanov/fastapi-best-practices#13-set-db-naming-convention)
14. [Set tests client async from day 0.](https://github.com/zhanymkanov/fastapi-best-practices#14-set-tests-client-async-from-day-0)
15. [BackgroundTasks > asyncio.create_task.](https://github.com/zhanymkanov/fastapi-best-practices#15-backgroundtasks--asynciocreate_task)
16. [Typing is important.](https://github.com/zhanymkanov/fastapi-best-practices#16-typing-is-important)
17. [Save files in chunks.](https://github.com/zhanymkanov/fastapi-best-practices#17-save-files-in-chunks)
18. [Be careful with dynamic pydantic fields.](https://github.com/zhanymkanov/fastapi-best-practices#18-be-careful-with-dynamic-pydantic-fields)
19. [SQL-first, Pydantic-second.](https://github.com/zhanymkanov/fastapi-best-practices#19-sql-first-pydantic-second) 
20. [Validate hosts, if users can send publicly available URLs.](https://github.com/zhanymkanov/fastapi-best-practices#20-validate-hosts-if-users-can-send-publicly-available-urls)
21. [Raise a ValueError in custom pydantic validators, if schema directly faces the client.](https://github.com/zhanymkanov/fastapi-best-practices#21-raise-a-valueerror-in-custom-pydantic-validators-if-schema-directly-faces-the-client)
22. [FastAPI converts Pydantic objects to dict, then to Pydantic object, then to JSON](https://github.com/zhanymkanov/fastapi-best-practices#22-fastapi-converts-pydantic-objects-to-dict-then-to-pydantic-object-then-to-json)
23. [If you must use sync SDK, then run it in a thread pool.](https://github.com/zhanymkanov/fastapi-best-practices#23-if-you-must-use-sync-sdk-then-run-it-in-a-thread-pool)
24. [Линтеры на проекте??? (black, ruff)](https://github.com/zhanymkanov/fastapi-best-practices#24-use-linters-black-isort-autoflake)
25. [Бонус](https://github.com/zhanymkanov/fastapi-best-practices#bonus-section)
<p style="text-align: center;"> <i>Project <a href="https://github.com/zhanymkanov/fastapi_production_template">sample</a> built with these best-practices in mind. </i> </p>

### 1. Структура проекта. Последовательная и предсказуемая
Существует много способов структурировать проект, но лучшая структура — это структура, которая является последовательной, простой и не содержит сюрпризов.
- Если изучение структуры проекта не дает вам представления о том, о чем он, возможно, структура неясна.
- Если вам приходится открывать пакеты, чтобы понять, какие модули в них расположены, значит ваша структура неясна.
- Если частота и расположение файлов кажутся случайными, значит, структура вашего проекта плоха.
- Если взгляд на расположение модуля и его название не дают вам представления о том, что у него внутри, значит, у вас очень плохая структура.

Также структура проекта, где разделяются файлы по их типам, (таким как api, crud, models, schemas)
представлена в [@tiangolo](https://github.com/tiangolo), которая подходит для микросервисов или проектов с меньшим количеством областей, 
ведь мы не можем поместить в монолит много модулей и доменов. 
Пример структуры компании Netflix [Dispatch](https://github.com/Netflix/dispatch) с небольшими дополнениями.
```
fastapi-project
├── alembic/
├── src
│   ├── auth
│   │   ├── router.py
│   │   ├── schemas.py  # pydantic models
│   │   ├── models.py  # db models
│   │   ├── dependencies.py
│   │   ├── config.py  # local configs
│   │   ├── constants.py
│   │   ├── exceptions.py
│   │   ├── service.py
│   │   └── utils.py
│   ├── aws
│   │   ├── client.py  # client model for external service communication
│   │   ├── schemas.py
│   │   ├── config.py
│   │   ├── constants.py
│   │   ├── exceptions.py
│   │   └── utils.py
│   └── posts
│   │   ├── router.py
│   │   ├── schemas.py
│   │   ├── models.py
│   │   ├── dependencies.py
│   │   ├── constants.py
│   │   ├── exceptions.py
│   │   ├── service.py
│   │   └── utils.py
│   ├── config.py  # global configs
│   ├── models.py  # global models
│   ├── exceptions.py  # global exceptions
│   ├── pagination.py  # global module e.g. pagination
│   ├── database.py  # db connection related stuff
│   └── main.py
├── tests/
│   ├── auth
│   ├── aws
│   └── posts
├── templates/
│   └── index.html
├── requirements
│   ├── base.txt
│   ├── dev.txt
│   └── prod.txt
├── .env
├── .gitignore
├── logging.ini
└── alembic.ini
```
1. Store all domain directories inside `src` folder
   1. `src/` - highest level of an app, contains common models, configs, and constants, etc.
   2. `src/main.py` - root of the project, which inits the FastAPI app
2. Each package has its own router, schemas, models, etc.
   1. `router.py` - is a core of each module with all the endpoints
   2. `schemas.py` - for pydantic models
   3. `models.py` - for db models
   4. `service.py` - module specific business logic  
   5. `dependencies.py` - router dependencies
   6. `constants.py` - module specific constants and error codes
   7. `config.py` - e.g. env vars
   8. `utils.py` - non-business logic functions, e.g. response normalization, data enrichment, etc.
   9. `exceptions.py` - module specific exceptions, e.g. `PostNotFound`, `InvalidUserData`
3. When package requires services or dependencies or constants from other packages - import them with an explicit module name
```python
from src.auth import constants as auth_constants
from src.notifications import service as notification_service
from src.posts.constants import ErrorCode as PostsErrorCode  # in case we have Standard ErrorCode in constants module of each package
```

### 2. PyDantic - круто! Ошибки лучше в ифах
Pydantic имеет богатый набор функций для проверки и преобразования данных. 

В дополнение к обычным функциям, таким как обязательные и необязательные поля со значениями по умолчанию, Pydantic имеет встроенные комплексные инструменты обработки данных, такие как регулярные выражения, перечисления для ограниченно разрешенных параметров, проверка длины, проверка электронной почты и т. д.
```python
from enum import Enum
from pydantic import AnyUrl, BaseModel, EmailStr, Field, constr

class MusicBand(str, Enum):
   AEROSMITH = "AEROSMITH"
   QUEEN = "QUEEN"
   ACDC = "AC/DC"


class UserBase(BaseModel):
    first_name: str = Field(min_length=1, max_length=128)
    username: constr(pattern="^[A-Za-z0-9-_]+$", to_lower=True, strip_whitespace=True)
    email: EmailStr
    age: int = Field(ge=18, default=None)  # must be greater or equal to 18
    favorite_band: MusicBand = (
        None  # only "AEROSMITH", "QUEEN", "AC/DC" values are allowed to be inputted
    )
    website: AnyUrl = None

```
### 3. Лучше использовать встраивание зависимостей вместо операций БД
Pydantic может проверять значения только на основе входных данных клиента.
Используйте зависимости для проверки данных на соответствие ограничениям базы данных, например, адрес электронной почты уже существует, пользователь не найден и т. д.
```python
# dependencies.py
async def valid_post_id(post_id: UUID4) -> Mapping:
    post = await service.get_by_id(post_id)
    if not post:
        raise PostNotFound()

    return post


# router.py
@router.get("/posts/{post_id}", response_model=PostResponse)
async def get_post_by_id(post: Mapping = Depends(valid_post_id)):
    return post


@router.put("/posts/{post_id}", response_model=PostResponse)
async def update_post(
    update_data: PostUpdate,  
    post: Mapping = Depends(valid_post_id), 
):
    updated_post: Mapping = await service.update(id=post["id"], data=update_data)
    return updated_post


@router.get("/posts/{post_id}/reviews", response_model=list[ReviewsResponse])
async def get_post_reviews(post: Mapping = Depends(valid_post_id)):
    post_reviews: list[Mapping] = await reviews_service.get_by_post_id(post["id"])
    return post_reviews
```
Если бы мы не включили проверку данных в зависимость, нам пришлось бы добавить проверку post_id.
для каждого эндпоинта и писать для каждого из них одинаковые тесты

### 4. Связанные зависимости
Зависимости могут использовать другие зависимости и вам нужно избегать повторения кода для аналогичной логики.
```python
# dependencies.py
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt

async def valid_post_id(post_id: UUID4) -> Mapping:
    post = await service.get_by_id(post_id)
    if not post:
        raise PostNotFound()

    return post


async def parse_jwt_data(
    token: str = Depends(OAuth2PasswordBearer(tokenUrl="/auth/token"))
) -> dict:
    try:
        payload = jwt.decode(token, "JWT_SECRET", algorithms=["HS256"])
    except JWTError:
        raise InvalidCredentials()

    return {"user_id": payload["id"]}


async def valid_owned_post(
    post: Mapping = Depends(valid_post_id), 
    token_data: dict = Depends(parse_jwt_data),
) -> Mapping:
    if post["creator_id"] != token_data["user_id"]:
        raise UserNotOwner()

    return post

# router.py
@router.get("/users/{user_id}/posts/{post_id}", response_model=PostResponse)
async def get_user_post(post: Mapping = Depends(valid_owned_post)):
    return post

```
### 5. Разделение и повторное использование зависимостей. Вызовы зависимостей кэшируются.
Зависимости можно использовать повторно несколько раз, и они не будут пересчитываться — FastAPI по умолчанию кэширует результат зависимости в области запроса.
то есть, если у нас есть зависимость, которая вызывает службу `get_post_by_id`, мы не будем посещать БД каждый раз, когда вызываем эту зависимость — только первый вызов функции.

Зная это, мы можем легко разделить зависимости на несколько более мелких функций, которые работают в меньшем домене и которые легче повторно использовать в других маршрутах.
Например, в приведенном ниже коде мы используем `parse_jwt_data` три раза:
1. `valid_owned_post`
2. `valid_active_creator`
3. `get_user_post`,

Но `parse_jwt_data` вызван только один раз в самом первом использовании.

```python
# dependencies.py
from fastapi import BackgroundTasks
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt

async def valid_post_id(post_id: UUID4) -> Mapping:
    post = await service.get_by_id(post_id)
    if not post:
        raise PostNotFound()

    return post


async def parse_jwt_data(
    token: str = Depends(OAuth2PasswordBearer(tokenUrl="/auth/token"))
) -> dict:
    try:
        payload = jwt.decode(token, "JWT_SECRET", algorithms=["HS256"])
    except JWTError:
        raise InvalidCredentials()

    return {"user_id": payload["id"]}


async def valid_owned_post(
    post: Mapping = Depends(valid_post_id), 
    token_data: dict = Depends(parse_jwt_data),
) -> Mapping:
    if post["creator_id"] != token_data["user_id"]:
        raise UserNotOwner()

    return post


async def valid_active_creator(
    token_data: dict = Depends(parse_jwt_data),
):
    user = await users_service.get_by_id(token_data["user_id"])
    if not user["is_active"]:
        raise UserIsBanned()
    
    if not user["is_creator"]:
       raise UserNotCreator()
    
    return user
        

# router.py
@router.get("/users/{user_id}/posts/{post_id}", response_model=PostResponse)
async def get_user_post(
    worker: BackgroundTasks,
    post: Mapping = Depends(valid_owned_post),
    user: Mapping = Depends(valid_active_creator),
):
    """Get post that belong the active user."""
    worker.add_task(notifications_service.send_email, user["id"])
    return post

```

### 6. Руководствуйтесь REST    ПОПРОБОВАТЬ
Разработка RESTful API упрощает повторное использование зависимостей в таких маршрутах:
   1. `GET /courses/:course_id`
   2. `GET /courses/:course_id/chapters/:chapter_id/lessons`
   3. `GET /chapters/:chapter_id`

Единственное ограничение — использовать в пути одни и те же имена переменных:
- Если у вас есть два эндпоинта `GET /profiles/:profile_id` и `GET /creators/:creator_id`
которые оба проверяют существование `profile_id` ,  но `GET /creators/:creator_id`
также проверяет  профиль создателя, тогда будет лучше переименовать значение пути `creator_id` в `profile_id` и связать эти две зависимости.
```python
# src.profiles.dependencies
async def valid_profile_id(profile_id: UUID4) -> Mapping:
    profile = await service.get_by_id(profile_id)
    if not profile:
        raise ProfileNotFound()

    return profile

# src.creators.dependencies
async def valid_creator_id(profile: Mapping = Depends(valid_profile_id)) -> Mapping:
    if not profile["is_creator"]:
       raise ProfileNotCreator()

    return profile

# src.profiles.router.py
@router.get("/profiles/{profile_id}", response_model=ProfileResponse)
async def get_user_profile_by_id(profile: Mapping = Depends(valid_profile_id)):
    """Get profile by id."""
    return profile

# src.creators.router.py
@router.get("/creators/{profile_id}", response_model=ProfileResponse)
async def get_user_profile_by_id(
     creator_profile: Mapping = Depends(valid_creator_id)
):
    """Get creator's profile by id."""
    return creator_profile

```

Используйте /me эндпоинты для ресурсов пользователя (таких как `GET /profiles/me`, `GET /users/me/posts`)
Плюсы:
   1. Нет необходимости проверять существование идентификатора пользователя — он уже проверен с помощью метода аутентификации.
   2. Нет необходимости проверять, принадлежит ли идентификатор пользователя запрашивающей стороне.

### 7. Не пишите асинхронно, если используете только I/O операции
Под капотом FastAPI может [эффективно](https://fastapi.tiangolo.com/async/#path-operation-functions) работать как с синхронными I/O, так и с асинхронными операциями. 
- FastAPI запускает `sync` маршруты в [threadpool](https://en.wikipedia.org/wiki/Thread_pool) 
и блокирующие I/O операции не остановят [event loop](https://docs.python.org/3/library/asyncio-eventloop.html) 
с выполнением задач. 
- В противном случае, если маршрут определен как `async`, он регулярно вызывается через `await`.
и FastAPI доверяет вам выполнять только неблокирующие операции ввода-вывода.

Предостережение: если вы подведете это доверие и выполните операции блокировки в асинхронных маршрутах,
цикл событий не сможет запускать следующие задачи, пока эта операция блокировки не будет выполнена.
```python
import asyncio
import time

@router.get("/terrible-ping")
async def terrible_catastrophic_ping():
    time.sleep(10) # I/O blocking operation for 10 seconds
    pong = service.get_pong()  # I/O blocking operation to get pong from DB
    
    return {"pong": pong}

@router.get("/good-ping")
def good_ping():
    time.sleep(10) # I/O blocking operation for 10 seconds, but in another thread
    pong = service.get_pong()  # I/O blocking operation to get pong from DB, but in another thread
    
    return {"pong": pong}

@router.get("/perfect-ping")
async def perfect_ping():
    await asyncio.sleep(10) # non-blocking I/O operation
    pong = await service.async_get_pong()  # non-blocking I/O db call

    return {"pong": pong}

```
**Что происходит когда мы вызываем:**
1. `GET /terrible-ping`
   1. Сервер FastAPI получает запрос и начинает его обрабатывать 
   2. Цикл событий сервера и все задачи в очереди будут ожидать завершения `time.sleep()`
      1. Сервер считает, что `time.sleep()` не является задачей ввода-вывода, поэтому он ждет, пока она не завершится.
      2. Сервер не будет принимать новые запросы во время ожидания
   3. Затем цикл событий и все задачи в очереди будут ждать завершения `service.get_pong`.
      1. Сервер считает, что `service.get_pong()` не является задачей ввода-вывода, поэтому он ждет, пока она не завершится.
      2. Сервер не будет принимать новые запросы во время ожидания
   4. Сервер возвращает ответ
      1. После ответа сервер начинает принимать новые запросы.
2. `GET /good-ping`
   1. Сервер FastAPI получает запрос и начинает его обрабатывать
   2. FastAPI отправляет весь маршрут «good_ping» в пул потоков, где рабочий поток выполнит функцию
   3. Пока выполняется `good_ping`, цикл событий выбирает следующие задачи из очереди и работает над ними (например, принять новый запрос, вызвать базу данных)
      - Независимо от основного потока (т. е. нашего приложения FastAPI),
        рабочий поток будет ждать завершения `time.sleep`, а затем завершения `service.get_pong`
      - Операция синхронизации блокирует только не основной поток.
   4. Когда `good_ping` завершает свою работу, сервер возвращает ответ клиенту.
3. `GET /perfect-ping`
   1. Сервер FastAPI получает запрос и начинает его обрабатывать
   2. FastAPI ждёт `asyncio.sleep(10)`
   3. Цикл событий выбирает следующие задачи из очереди и работает над ними (например, принять новый запрос, вызвать базу данных)
   4. Когда `asyncio.sleep(10)` завершен, серверы переходят к следующим строкам и ожидают `service.async_get_pong`
   5. Цикл событий выбирает следующие задачи из очереди и работает над ними (например, принять новый запрос, вызвать базу данных)
   6. Когда `service.async_get_pong` завершен, сервер возвращает ответ клиенту.

Второе предостережение заключается в том, что операции, которые являются неблокирующими ожидаемыми или отправляются в пул потоков, должны быть задачами с 
интенсивным вводом-выводом (например, открытие файла, вызов базы данных, вызов внешнего API).
- Ожидание задач, интенсивно использующих ЦП (например, тяжелые вычисления, обработка данных, перекодирование видео), бесполезно, поскольку ЦП должен работать для завершения задач.
в то время как операции ввода-вывода являются внешними, и сервер ничего не делает, ожидая завершения этих операций, поэтому он может перейти к следующим задачам.
-Выполнение ресурсоемких задач в других потоках также неэффективно из-за [GIL](https://realpython.com/python-gil/). 
Короче говоря, GIL позволяет одновременно работать только одному потоку, что делает его бесполезным для задач ЦП.
- Если вы хотите оптимизировать задачи, интенсивно использующие процессор, вам следует отправить их воркерам в другой процесс.

**Связанные вопросы StackOverflow от запутавшихся пользователей**
1. https://stackoverflow.com/questions/62976648/architecture-flask-vs-fastapi/70309597#70309597
2. https://stackoverflow.com/questions/65342833/fastapi-uploadfile-is-slow-compared-to-flask
3. https://stackoverflow.com/questions/71516140/fastapi-runs-api-calls-in-serial-instead-of-parallel-fashion

### 8. Глобальные BaseModel с начала проекта.
Наличие управляемой глобальной базовой модели позволяет нам настраивать все модели в приложении.
Например, мы могли бы использовать стандартный формат даты и времени или добавить суперметод для всех подклассов базовой модели.
```python
from datetime import datetime
from typing import Any
from zoneinfo import ZoneInfo

from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, ConfigDict, model_validator


def convert_datetime_to_gmt(dt: datetime) -> str:
    if not dt.tzinfo:
        dt = dt.replace(tzinfo=ZoneInfo("UTC"))

    return dt.strftime("%Y-%m-%dT%H:%M:%S%z")


class CustomModel(BaseModel):
    model_config = ConfigDict(
        json_encoders={datetime: convert_datetime_to_gmt},
        populate_by_name=True,
    )

    @model_validator(mode="before")
    @classmethod
    def set_null_microseconds(cls, data: dict[str, Any]) -> dict[str, Any]:
        datetime_fields = {
            k: v.replace(microsecond=0)
            for k, v in data.items()
            if isinstance(k, datetime)
        }

        return {**data, **datetime_fields}

    def serializable_dict(self, **kwargs):
        """Return a dict which contains only serializable fields."""
        default_dict = self.model_dump()

        return jsonable_encoder(default_dict)

```
В приведенном выше примере мы решили создать глобальную базовую модель, которая: 
- сбрасывает микросекунды до 0 во всех форматах даты
- сериализует все поля даты и времени в стандартный формат с явным часовым поясом 
### 9. Docs

1. Помогите FastAPI сгенерировать понятную документацию
   1. Установить `response_model`, `status_code`, `description`, и другие.
   2. Если модели и статусы различаются, используйте атрибут маршрута `responses`, чтобы добавить документы для разных ответов.
```python
from fastapi import APIRouter, status

router = APIRouter()

@router.post(
    "/endpoints",
    response_model=DefaultResponseModel,  # default response pydantic model 
    status_code=status.HTTP_201_CREATED,  # default status code
    description="Description of the well documented endpoint",
    tags=["Endpoint Category"],
    summary="Summary of the Endpoint",
    responses={
        status.HTTP_200_OK: {
            "model": OkResponse, # custom pydantic model for 200 response
            "description": "Ok Response",
        },
        status.HTTP_201_CREATED: {
            "model": CreatedResponse,  # custom pydantic model for 201 response
            "description": "Creates something from user request ",
        },
        status.HTTP_202_ACCEPTED: {
            "model": AcceptedResponse,  # custom pydantic model for 202 response
            "description": "Accepts request and handles it later",
        },
    },
)
async def documented_route():
    pass
```
Сгенерирует следующее:
![FastAPI DOCS](images/custom_responses.png "Custom Response Docs")

### 10. Используйте Pydantic's BaseSettings для конфигурации
Pydantic позволяет использовать [крутой инструмент](https://pydantic-docs.helpmanual.io/usage/settings/) для парсинга переменных окружения и взаимодействия с их валидатором. 
```python
from pydantic import AnyUrl, PostgresDsn
from pydantic_settings import BaseSettings  # pydantic v2

class AppSettings(BaseSettings):
    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        env_prefix = "app_"

    DATABASE_URL: PostgresDsn
    IS_GOOD_ENV: bool = True
    ALLOWED_CORS_ORIGINS: set[AnyUrl]
```
### 11. SQLAlchemy: Руководствуйтесь конвенцией наименования ключей БД
Явная установка именования индексов в соответствии с соглашением вашей базы данных предпочтительнее, чем sqlalchemy.
```python
from sqlalchemy import MetaData

POSTGRES_INDEXES_NAMING_CONVENTION = {
    "ix": "%(column_0_label)s_idx",
    "uq": "%(table_name)s_%(column_0_name)s_key",
    "ck": "%(table_name)s_%(constraint_name)s_check",
    "fk": "%(table_name)s_%(column_0_name)s_fkey",
    "pk": "%(table_name)s_pkey",
}
metadata = MetaData(naming_convention=POSTGRES_INDEXES_NAMING_CONVENTION)
```
### 12. Migrations. Alembic. ????
1. Migrations must be static and revertable.
If your migrations depend on dynamically generated data, then 
make sure the only thing that is dynamic is the data itself, not its structure.
2. Generate migrations with descriptive names & slugs. Slug is required and should explain the changes.
3. Set human-readable file template for new migrations. We use `*date*_*slug*.py` pattern, e.g. `2022-08-24_post_content_idx.py`
```
# alembic.ini
file_template = %%(year)d-%%(month).2d-%%(day).2d_%%(slug)s
```
### 13. Руководствуйтесь конвенцией БД для наименований
Важно соблюдать последовательность в именах. Некоторые правила, которым мы следовали:
1. lower_case_snake
2. singular form (e.g. `post`, `post_like`, `user_playlist`)
3. группировать похожие таблицы с префиксом модуля, e.g. `payment_account`, `payment_bill`, `post`, `post_like`
4. сохраняйте единообразие во всех таблицах, но можно использовать конкретные имена, например:
   1. Используйте `profile_id` во всех таблицах, но если некоторым из них нужны только профили-создатели, используйте `creator_id`
   2. Используйте `post_id` для всех абстрактных таблиц, таких как `post_like`, `post_view`, но используйте конкретные имена в соответствующих модулях, например `course_id` в `chapters.course_id`
5. `_at` суффикс для datetime
6. `_date` суффикс для date

### 14. Set tests client async from day 0 ???
Writing integration tests with DB will most likely lead to messed up event loop errors in the future.
Set the async test client immediately, e.g. [async_asgi_testclient](https://github.com/vinissimus/async-asgi-testclient) or [httpx](https://github.com/encode/starlette/issues/652)
```python
import pytest
from async_asgi_testclient import TestClient

from src.main import app  # inited FastAPI app


@pytest.fixture
async def client():
    host, port = "127.0.0.1", "5555"
    scope = {"client": (host, port)}

    async with TestClient(
        app, scope=scope, headers={"X-User-Fingerprint": "Test"}
    ) as client:
        yield client


@pytest.mark.asyncio
async def test_create_post(client: TestClient):
    resp = await client.post("/posts")

    assert resp.status_code == 201
```
Unless you have sync db connections (excuse me?) or aren't planning to write integration tests.
### 15. BackgroundTasks > asyncio.create_task
BackgroundTasks могут [эффективно работать](https://github.com/encode/starlette/blob/31164e346b9bd1ce17d968e1301c3bb2c23bb418/starlette/background.py#L25)
одновременно как блокирующие, так и неблокирующие операции ввода-вывода точно так же, как FastAPI обрабатывает блокирующие маршруты (задачи синхронизации выполняются в пуле потоков, а задачи асинхронности ожидаются позже)
- Не отмечайте блокирующие операции ввода-вывода как «асинхронные».
- Не используйте их для задач с интенсивным использованием процессора.
```python
from fastapi import APIRouter, BackgroundTasks
from pydantic import UUID4

from src.notifications import service as notifications_service


router = APIRouter()


@router.post("/users/{user_id}/email")
async def send_user_email(worker: BackgroundTasks, user_id: UUID4):
    """Send email to user"""
    worker.add_task(notifications_service.send_email, user_id)  # send email after responding client
    return {"status": "ok"}
```
### 16. Type Hinting
FastAPI, Pydantic и современные IDE поощряют использование подсказок типов.

**Без Type Hints**

<img src="images/type_hintsless.png" width="400" height="auto">

**С Type Hints**

<img src="images/type_hints.png" width="400" height="auto">

### 17. Сохраняйте файлы в чанки. Добавтьб
Не надейтесь, что ваши клиенты будут отправлять небольшие файлы.
```python
import aiofiles
from fastapi import UploadFile

DEFAULT_CHUNK_SIZE = 1024 * 1024 * 50  # 50 megabytes

async def save_video(video_file: UploadFile):
   async with aiofiles.open("/file/path/name.mp4", "wb") as f:
     while chunk := await video_file.read(DEFAULT_CHUNK_SIZE):
         await f.write(chunk)
```
### 18. Будьте осторожны с динамическими полями Pydantic (Pydantic v1).
Если у вас есть pydantic поле, которое может принимать объединение типов, убедитесь, что валидатор явно знает разницу между этими типами.
```python
from pydantic import BaseModel


class Article(BaseModel):
   text: str | None
   extra: str | None


class Video(BaseModel):
   video_id: int
   text: str | None
   extra: str | None

   
class Post(BaseModel):
   content: Article | Video

   
post = Post(content={"video_id": 1, "text": "text"})
print(type(post.content))
# OUTPUT: Article
# Статья очень информативна, и все поля являются необязательными, что позволяет любому словарю существовать.
```
**Решения:**
1. Проверка ввода допускает только допустимые поля и вызывает ошибку, если указаны неизвестные поля.
```python
from pydantic import BaseModel, Extra

class Article(BaseModel):
   text: str | None
   extra: str | None
   
   class Config:
        extra = Extra.forbid
       

class Video(BaseModel):
   video_id: int
   text: str | None
   extra: str | None
   
   class Config:
        extra = Extra.forbid

   
class Post(BaseModel):
   content: Article | Video
```
2. Используйте Smart Union от Pydantic (>v1.9, <2.0), если поля простые.

Это хорошее решение, если поля простые, например, `int` или `bool`.
но это не работает для сложных полей, таких как классы.

Без Smart Union
```python
from pydantic import BaseModel


class Post(BaseModel):
   field_1: bool | int
   field_2: int | str
   content: Article | Video

p = Post(field_1=1, field_2="1", content={"video_id": 1})
print(p.field_1)
# OUTPUT: True
print(type(p.field_2))
# OUTPUT: int
print(type(p.content))
# OUTPUT: Article
```
С использованием Smart Union
```python
class Post(BaseModel):
   field_1: bool | int
   field_2: int | str
   content: Article | Video

   class Config:
      smart_union = True


p = Post(field_1=1, field_2="1", content={"video_id": 1})
print(p.field_1)
# OUTPUT: 1
print(type(p.field_2))
# OUTPUT: str
print(type(p.content))
# OUTPUT: Article, because smart_union doesn't work for complex fields like classes
```

3. Быстрое решение

Правильно расставляйте типы полей: от самых строгих до публичных.

```python
class Post(BaseModel):
   content: Video | Article
```

### 19. Сначала SQL, потом Pydantic
- Обычно база данных обрабатывает данные намного быстрее и чище, чем когда-либо делал CPython.
- Предпочтительно выполнять все сложные соединения и простые манипуляции с данными с помощью SQL.
- Предпочтительно агрегировать JSON в БД для ответов с вложенными объектами.
```python
# src.posts.service
from typing import Mapping

from pydantic import UUID4
from sqlalchemy import desc, func, select, text
from sqlalchemy.sql.functions import coalesce

from src.database import database, posts, profiles, post_review, products

async def get_posts(
    creator_id: UUID4, *, limit: int = 10, offset: int = 0
) -> list[Mapping]: 
    select_query = (
        select(
            (
                posts.c.id,
                posts.c.type,
                posts.c.slug,
                posts.c.title,
                func.json_build_object(
                   text("'id', profiles.id"),
                   text("'first_name', profiles.first_name"),
                   text("'last_name', profiles.last_name"),
                   text("'username', profiles.username"),
                ).label("creator"),
            )
        )
        .select_from(posts.join(profiles, posts.c.owner_id == profiles.c.id))
        .where(posts.c.owner_id == creator_id)
        .limit(limit)
        .offset(offset)
        .group_by(
            posts.c.id,
            posts.c.type,
            posts.c.slug,
            posts.c.title,
            profiles.c.id,
            profiles.c.first_name,
            profiles.c.last_name,
            profiles.c.username,
            profiles.c.avatar,
        )
        .order_by(
            desc(coalesce(posts.c.updated_at, posts.c.published_at, posts.c.created_at))
        )
    )
    
    return await database.fetch_all(select_query)

# src.posts.schemas
import orjson
from enum import Enum

from pydantic import BaseModel, UUID4, validator


class PostType(str, Enum):
    ARTICLE = "ARTICLE"
    COURSE = "COURSE"

   
class Creator(BaseModel):
    id: UUID4
    first_name: str
    last_name: str
    username: str


class Post(BaseModel):
    id: UUID4
    type: PostType
    slug: str
    title: str
    creator: Creator

    @validator("creator", pre=True)  # before default validation
    def parse_json(cls, creator: str | dict | Creator) -> dict | Creator:
       if isinstance(creator, str):  # i.e. json
          return orjson.loads(creator)

       return creator
    
# src.posts.router
from fastapi import APIRouter, Depends

router = APIRouter()


@router.get("/creators/{creator_id}/posts", response_model=list[Post])
async def get_creator_posts(creator: Mapping = Depends(valid_creator_id)):
   posts = await service.get_posts(creator["id"])

   return posts
```

Если БД в форме агрегированных данных представляет собой простой JSON, взгляните на тип поля `Json` в Pydantic,
который сначала загрузит необработанный JSON.
```python
from pydantic import BaseModel, Json

class A(BaseModel):
    numbers: Json[list[int]]
    dicts: Json[dict[str, int]]

valid_a = A(numbers="[1, 2, 3]", dicts='{"key": 1000}')  # becomes A(numbers=[1,2,3], dicts={"key": 1000})
invalid_a = A(numbers='["a", "b", "c"]', dicts='{"key": "str instead of int"}')  # raises ValueError
```

### 20. Проверьте хосты, могут ли пользователи отправлять общедоступные URL-адреса.
Например, у нас есть эндпоинт, который:
1. принимает медиафайл от пользователя,
2. генерирует уникальный URL-адрес для этого файла,
3. возвращает URL пользователю,
   1. который они будут использовать в других эндпоинтах, таких как `PUT /profiles/me`, `POST /posts`
   2. эти эндпоинты принимают файлы только с хостов из белого списка
4. загружает файл на AWS с этим именем и соответствующим URL-адресом.
5. 
Если мы не внесем URL-хосты в белый список, у злоумышленников будет возможность навредить сервису.
```python
from pydantic import AnyUrl, BaseModel

ALLOWED_MEDIA_URLS = {"mysite.com", "mysite.org"}

class CompanyMediaUrl(AnyUrl):
    @classmethod
    def validate_host(cls, parts: dict) -> tuple[str, str, str, bool]:  # pydantic v1
       """Extend pydantic's AnyUrl validation to whitelist URL hosts."""
        host, tld, host_type, rebuild = super().validate_host(parts)
        if host not in ALLOWED_MEDIA_URLS:
            raise ValueError(
                "Forbidden host url. Upload files only to internal services."
            )

        return host, tld, host_type, rebuild


class Profile(BaseModel):
    avatar_url: CompanyMediaUrl  # only whitelisted urls for avatar

```
### 21. Вызовите ValueError в пользовательских валидаторах pydantic, если схема напрямую обращена к клиенту.
Он вернет хороший подробный ответ пользователям.
```python
# src.profiles.schemas
from pydantic import BaseModel, validator

class ProfileCreate(BaseModel):
    username: str
    
    @validator("username")  # pydantic v1
    def validate_bad_words(cls, username: str):
        if username  == "me":
            raise ValueError("bad username, choose another")
        
        return username


# src.profiles.routes
from fastapi import APIRouter

router = APIRouter()


@router.post("/profiles")
async def get_creator_posts(profile_data: ProfileCreate):
   pass
```
**Пример ответа:**

<img src="images/custom_bad_response.png" width="400" height="auto">

### 22. FastAPI преобразует объекты Pydantic в dict, затем в объект Pydantic, а затем в JSON.
Если вы считаете, что можете вернуть объект Pydantic, соответствующий `response_model` вашего маршрута, чтобы выполнить некоторую оптимизацию,
тогда это неправильно.

FastAPI сначала преобразует этот объект pydantic в dict с помощью jsonable_encoder, а затем проверяет
данные с вашей `response_model` и только затем сериализует ваш объект в JSON.
```python
from fastapi import FastAPI
from pydantic import BaseModel, root_validator

app = FastAPI()


class ProfileResponse(BaseModel):
    @root_validator
    def debug_usage(cls, data: dict):
        print("created pydantic model")

        return data

    def dict(self, *args, **kwargs):
        print("called dict")
        return super().dict(*args, **kwargs)


@app.get("/", response_model=ProfileResponse)
async def root():
    return ProfileResponse()
```
**Logs Output:**
```
[INFO] [2022-08-28 12:00:00.000000] created pydantic model
[INFO] [2022-08-28 12:00:00.000010] called dict
[INFO] [2022-08-28 12:00:00.000020] created pydantic model
[INFO] [2022-08-28 12:00:00.000030] called dict
```
### 23. Если вам необходимо использовать SDK синхронизации, запустите его в пуле потоков.
Если вам необходимо использовать библиотеку для взаимодействия с внешними службами, и она не асинхронна,
тогда выполните HTTP-вызовы во внешнем рабочем потоке.

В качестве простого примера мы могли бы использовать наш хорошо известный `run_in_threadpool` от Starlette.
```python
from fastapi import FastAPI
from fastapi.concurrency import run_in_threadpool
from my_sync_library import SyncAPIClient 

app = FastAPI()


@app.get("/")
async def call_my_sync_library():
    my_data = await service.get_my_data()

    client = SyncAPIClient()
    await run_in_threadpool(client.make_request, data=my_data)
```
### 24. Линтеры на проекте??? (black, ruff)
With linters, you can forget about formatting the code and focus on writing the business logic.

Black is the uncompromising code formatter that eliminates so many small decisions you have to make during development.
Ruff is "blazingly-fast" new linter that replaces autoflake and isort, and supports more than 600 lint rules.

It's a popular good practice to use pre-commit hooks, but just using the script was ok for us.
```shell
#!/bin/sh -e
set -x

ruff --fix
black src tests
```
### Бонус

Лучшие практики по стилизации проекта: [lowercase00](https://github.com/zhanymkanov/fastapi-best-practices/issues/4) 
подробно описал свои лучшие практики работы с разрешениями и аутентификацией, сервисами и представлениями на основе классов,
очереди задач, пользовательские сериализаторы ответов, настройка с помощью dynaconf и т. д.