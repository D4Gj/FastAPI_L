from fastapi import FastAPI, Request, Form, Cookie, Header, status
from fastapi.encoders import jsonable_encoder
from fastapi.exception_handlers import request_validation_exception_handler
from fastapi.exceptions import RequestValidationError
from fastapi.responses import (
    Response,
    HTMLResponse,
    StreamingResponse,
    FileResponse,
    RedirectResponse,
)
from fastapi.templating import Jinja2Templates
from fastapi.responses import JSONResponse
from pydantic.error_wrappers import ErrorWrapper
from models import *
from typing import Optional


template = Jinja2Templates(directory="templates")

app = FastAPI()


@app.exception_handler(RequestValidationError)
async def http_exception_accept_handler(
    request: Request, exc: RequestValidationError
) -> Response:
    print(exc._errors)
    print(exc)
    print(exc.errors())
    return await request_validation_exception_handler(request, exc)


# @app.exception_handler(RequestValidationError)
# async def http_exception_accept_handler(
#     request: Request, exc: RequestValidationError
# ) -> Response:
#     raw_errors = exc.errors
#     error_wrapper: ErrorWrapper = raw_errors[0]
#     validation_error: ValidationError = error_wrapper.exc
#     overwritten_errors = validation_error.errors()
#     return JSONResponse(
#         status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
#         content={"detail": jsonable_encoder(overwritten_errors)},
#     )


@app.get("/", response_class=HTMLResponse)
async def index(request: Request, user: str = Cookie(None)):
    return template.TemplateResponse("index.html", {"request": request, "user": user})


async def generator():
    for i in range(1, 99999999):
        yield f"Line {i}"


@app.post("/product/", response_model=ProductVal)
async def new_product(product: Product):
    product.Inventory_val = product.price * product.stock
    return product


@app.get("/red", response_class=HTMLResponse)
async def index(request: Request):
    return RedirectResponse("/file")


@app.get("/file", response_class=FileResponse)
async def file():
    file = "V4VY.gif"
    return file


@app.get("/streaming")
async def main():
    return StreamingResponse(generator())


@app.get("/header/")
async def set_header():
    content = {"message": "hello"}
    headers = {"X-Web-Framework": "FastAPI", "Content-Language": "en-US"}
    return JSONResponse(content=content, headers=headers)


@app.get("/read_header/", status_code=status.HTTP_204_NO_CONTENT)
async def read_header(accept_language: Optional[str] = Header(None)):
    return {"Language": accept_language}


@app.post("/setcookie/")
async def set_cookie(
    request: Request, response: Response, user: str = Form(...), pwd: str = Form(...)
):
    response.set_cookie(key="user", value=user)
    return {"message": "Hello"}


@app.post(
    "/user",
    description="Тут должно было быть",
    response_model=UserBase,
    summary="Итог",
    status_code=status.HTTP_201_CREATED,
)
async def get_user(user: UserBase):
    return user
