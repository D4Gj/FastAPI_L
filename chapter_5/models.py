from enum import Enum
from pydantic import EmailStr, Field, AnyUrl, constr, ValidationError
from pydantic_translations import Translator
from pydantic import BaseModel as PydanticBase

tr = Translator("ru")


class BaseModel(PydanticBase):
    class Config:
        error_msg_templates = {
            "value_error.email": "Bad main",
        }


class Product(BaseModel):
    prodId: int
    prodName: str
    price: float
    stock: int
    Inventory_val: float


class ProductVal(BaseModel):
    prodId: int
    prodName: str
    Inventory_val: float


class MusicBand(str, Enum):
    AEROSMITH = "AEROSMITH"
    QUEEN = "QUEEN"
    ACDC = "AC/DC"


class UserBase(BaseModel):
    first_name: str = Field(min_length=1, max_length=128)
    username: constr(regex="^[A-Za-z0-9-_]+$", to_lower=True, strip_whitespace=True)
    email: EmailStr
    age: int = Field(ge=18, default=None)  # must be greater or equal to 18
    favorite_band: MusicBand = (
        None  # only "AEROSMITH", "QUEEN", "AC/DC" values are allowed to be inputted
    )
    website: AnyUrl = None

    class Config:
        from_attributes = True
        error_msg_templates = {
            "value_error.email": "Bad main",
        }
