from fastapi import FastAPI, Path, Query
from typing import Optional

app = FastAPI()


@app.get("/")
async def index():
    return {"message": "Hello"}


@app.get("test/{name}/{id}")
async def user(name: str, id: int):
    return {"name": name, "id": id}


@app.get("/employee/{name}")
async def get_user(name: str, age: Optional[int] = None):
    return {"name": name, "age": age}


@app.get("/employee/{EmpName}/branch/{branch_id}")
async def get_employee(
    branch_id: int,
    name: str = Path(
        title="Nameqwe",
        description="ну его как-то зовут(не больше 10 символов в имени)",
        alias="EmpName",
        regex="^[J]|[h]$",
        max_length=10,
    ),
    brname: str = Query(None, min_length=5),
    age: int = Query(None, ge=20, lt=61, include_in_schema=False),  # gt ge lt le
):
    employee = {"name": name, "Branch": brname, "Branch ID": branch_id, "age": age}
    return employee
