from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()
templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


# @app.get("/{name}", response_class=HTMLResponse)
# async def index(name):
#     ret = f"""
# <html>
# <body>
# <h2>Hello {name}!</h2>
# </body>
# </html>
#     """
#     return Response(content=ret)
@app.get("/{name}", response_class=HTMLResponse)
async def index(request: Request, name: str):
    return templates.TemplateResponse("hello.html", {"request": request, "name": name})


@app.get("/employee/{name}/{salary}", response_class=HTMLResponse)
async def employee(request: Request, name: str, salary: int):
    data = {"name": name, "salary": salary}
    return templates.TemplateResponse(
        "employee.html", {"request": request, "data": data}
    )


@app.get("/testjs/{name}", response_class=HTMLResponse)
async def jsdemo(request: Request, name: str):
    data = {"name": name}
    return templates.TemplateResponse(
        "static-js.html", {"request": request, "data": data}
    )
